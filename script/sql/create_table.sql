﻿create table if not exists autorzy 
(
id int not null identity,
nazwa varchar(255) not null unique
);

create table if not exists wydawnictwo
(
id int not null identity,
nazwa varchar(255) not null unique
);

create table if not exists czytelnicy 
(
id integer not null identity,
imie varchar(255) not null,
nazwisko varchar(255) not null,
pesel char(11) not null unique,
email varchar(255) not null,
adresUlica varchar(255) not null,
adresNrDomu char(10) not null,
adresNrMieszkania char(10) default null,
miasto char(30) not null
 );
 
 

create table if not exists ksiazki 
(
id integer not null identity,
id_autor int not null,
data_wydania date not null,
tytul varchar(255) not null,
isbn char(20) not null unique,
id_wydawnictwo int not null,
liczba_stron int not null,
czy_wyp boolean default FALSE,

foreign key (id_autor) references autorzy(id),
foreign key (id_wydawnictwo) references wydawnictwo(id)
 );
 
create table if not exists wypozyczenie
(
id int not null identity,
id_ks int not null,
id_czyt int not null,
data_wyp date not null,
data_zw date not null,

foreign key(id_ks) references ksiazki(id),
foreign key(id_czyt) references czytelnicy(id)
);

