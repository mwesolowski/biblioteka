package pl.biblioteka.ui;

public interface UserInterface {
  
    /**
     * Prezentuje userowi komunikat.
     * @param string
     */
    void showMsg(String msg);
    
    /**
     * Prezentuje userowi komunikat błędu.
     * @param string
     */
    void showErrorMsg(String msg);
    
}
