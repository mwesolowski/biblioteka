package pl.biblioteka.ui.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import pl.biblioteka.entities.Czytelnik;
import pl.biblioteka.entityManagers.CzytelnikManager;
import pl.biblioteka.ui.UserInterface;
import pl.biblioteka.ui.action.ManageReadersAction;

public class ManageReadersMenu implements UserInterface {

	private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	private static volatile ManageReadersMenu instance;
	
	private ManageReadersMenu() {
		
	}
	
	public static ManageReadersMenu instance() {
		if (instance == null) {
			instance = new ManageReadersMenu();
		}
		
		return instance;
	}
	

	public void showGreeting() {
		System.out.println("" +
				"+--------------------------------------------+\n"+
                "|           Zarządzanie czytelnikami         |\n"+
                "+--------------------------------------------+");
		
	}
	
	public void go() {
		showGreeting();
		while (doOneAction()) {}
	}

	public ManageReadersAction showMenuAndGetAction() {
		System.out.println("\nCo checsz zrobić?\n" +
                "1. Wyświetl dane wszystkich czytelników.\n" +
				"2. Zmienić dane czytelnika.\n" +
                "3. Dodać nowego czytelnika. \n" + 
                "4. Usunąć dane czytelnika.\n" +
				"5. Wyszukać czytelnika po nr PESEL.\n" +
                "6. Wyszukać czytelnika po nazwisku.\n" +
				"7. Powrócić do poprzedniego menu.\n" +
                "(wpisz cyfrę od 1 do 7)");
		String choice = getUserInput();
		return ManageReadersAction.parse(choice);
	}
	
	private String getUserInput() {
        try {
                return br.readLine();
        } catch (IOException e) {
                System.err.println("Problem z wczytywaniem odpowiedzi użytkownika");
                System.exit(-1);
                return null;
        }
}

    public void showMsg(String msg) {
            System.out.println(msg);
    }
    
    /**
     * {@inheritDoc}
     */     
    public void showErrorMsg(String msg) {
            System.err.println("[BŁĄD] "+msg+"\n");
    }
    
    public boolean doOneAction() {
        
        ManageReadersAction action = null;
        do {
        	if (action == ManageReadersAction.INVALID){
        		showGreeting();
        	}
                action = showMenuAndGetAction();
        } while (action == ManageReadersAction.INVALID);
        
        switch (action) {
        case READERS_GETALL :
        	showAllReaders();
        	showGreeting();
        	    break;
        case READERS_EDIT :
        	editReader();
        	showGreeting();
                break;
        case READERS_ADD : 
        	addReader();
        	showGreeting();
        		break;
        case READERS_DELETE :
        	deleteCzytelnik();
        	showGreeting();
                break;
        case READERS_SEARCH_BY_PESEL : 
        	searchCzytelnikByPesel();
        	showGreeting();
        		break;
        case READERS_SEARCH_BY_NAME :
        	searchCzytelnikByNazwisko();
        	showGreeting();
        	break;
        case PREVIOUS :
                MainMenu.instance().go();
        default: return false;
        }
        
        return true;
    }
    

	private void showAllReaders() {
    	CzytelnikManager.instance().showAllCzytelnicy();
    }
    
    private void addReader() {
 	
    	System.out.println("Podaj imię: ");
    	String imie = getUserInput();
    	System.out.println("Podaj nazwisko:");
    	String nazwisko = getUserInput();
    	System.out.println("Podaj PESEL:");
    	String pesel = getUserInput();
    	System.out.println("Podaj E-mail:");
    	String email = getUserInput();
    	System.out.println("Podaj ulicę:");
    	String ulica = getUserInput();
    	System.out.println("Podaj nr domu:");
    	String nrdomu = getUserInput();
    	System.out.println("Podaj nr mieszkania (pole nieobowiązkowe):");
    	String nrmieszkania = getUserInput();
    	System.out.println("Podaj miasto:");
    	String miasto = getUserInput();
    	
    	CzytelnikManager.instance().addCzytelnik(imie, nazwisko, pesel, email, ulica, nrdomu, nrmieszkania, miasto);
    }
    
    private void editReader() {
    	
    	try {
    		System.out.println("Podaj ID czytelnika do zmiany:");
        	int idCzytelnika = Integer.valueOf(getUserInput());
        	
        	Czytelnik oldCz = CzytelnikManager.instance().findCzytelnik(idCzytelnika);
        	Czytelnik newCz = new Czytelnik();
        	
        	if (oldCz == null)
        	{
        		
        	} else {
        		System.out.println("Wpisz nowe imię (lub zostaw puste aby nie zmienić):");
        		newCz.setImie(getUserInput().trim());
        		
        		System.out.println("Wpisz nowe nazwisko (lub zostaw puste aby nie zmienić):");
        		newCz.setNazwisko(getUserInput().trim());
        		
        		System.out.println("Wpisz nowy PESEL (lub zostaw puste aby nie zmienić):");
        		newCz.setPesel(getUserInput().trim());
        		
        		System.out.println("Wpisz nowy email (lub zostaw puste aby nie zmienić):");
        		newCz.setEmail(getUserInput().trim());
        		
        		System.out.println("Wpisz nową ulicę (lub zostaw puste aby nie zmienić):");
        		newCz.setAdresUlica(getUserInput().trim());
        		
        		System.out.println("Wpisz nowe nr domu (lub zostaw puste aby nie zmienić):");
        		newCz.setAdresNrDomu(getUserInput().trim());
        		
        		System.out.println("Wpisz nowe nr mieszkania (lub zostaw puste aby nie zmienić):");
        		newCz.setAdresNrMieszkania(getUserInput().trim());
        		
        		System.out.println("Wpisz nowe miasto (lub zostaw puste aby nie zmienić):");
        		newCz.setAdresMiasto(getUserInput().trim());
        		
        		CzytelnikManager.instance().updateCzytelnik(idCzytelnika, newCz);
        	}
    	} catch (NumberFormatException e) {
    		System.out.println("Nieprawidłowy format ID.");
    	}
    	
    	
    }

    private void deleteCzytelnik() {
    	try {
    		System.out.println("Podaj ID czytelnika do usunięcia.");
    		int idCzytelnika = Integer.valueOf(getUserInput());
    		
    		CzytelnikManager.instance().deleteCzytelnik(idCzytelnika);
    	} catch (NumberFormatException e) {
    		System.out.println("Nieprawidłowy format ID.");
    	}
    }
    
    private void searchCzytelnikByNazwisko() {
    	System.out.println("Podaj nazwisko czytelnika do wyszukania:");
    	String czytNazwiskoToSearch = getUserInput();
    	
    	CzytelnikManager.instance().findCzytelnikByNazwisko(czytNazwiskoToSearch);
    	
    	
    }
    
    private void searchCzytelnikByPesel() {
    	System.out.println("Podaj PESEL czytelnika do wyszukania:");
    	String czytPeselToSearch = getUserInput();
    	
    	CzytelnikManager.instance().findCzytelnik(czytPeselToSearch);
		
	}
}
