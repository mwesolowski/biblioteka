package pl.biblioteka.ui.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import pl.biblioteka.entityManagers.WydawnictwoManager;
import pl.biblioteka.ui.UserInterface;
import pl.biblioteka.ui.action.ManagePublishAction;

public class ManagePublishMenu implements UserInterface {

	private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	private WydawnictwoManager wm = WydawnictwoManager.instance();
	
	private static volatile ManagePublishMenu instance;
	
	private ManagePublishMenu() {
		
	}
	
	public static ManagePublishMenu instance() {
		if (instance == null) {
			instance = new ManagePublishMenu();
		}
		
		return instance;
	}
	

	public void showGreeting() {
		System.out.println(
				"+---------------------------------------------+\n"+
                "|           Zarządzanie wydawnictwami         |\n"+
                "+---------------------------------------------+");
		
	}
	
	public void go() {
		showGreeting();
		while (doOneAction()) {}
	}

	public ManagePublishAction showMenuAndGetAction() {
		System.out.println("\nCo checsz zrobić?\n" +
				"1. Wyświetlić wszystkie wydawnictwa istniejące w bazie.\n" +
                "2. Dodać nowe wydawnictwo.\n" + 
				"3. Zmienić dane wydawnictwa.\n" + 
                "4. Wyszukać w bazie wydawnictwo.\n" + 
				"5. Usunąć wydawnictwo z bazy.\n" +
                "6. Wrócić do poprzedniego menu.\n" +
                "(wpisz cyfrę od 1 do 6)");
		String choice = getUserInput();
		return ManagePublishAction.parse(choice);
	}
	
	private String getUserInput() {
        try {
                return br.readLine();
        } catch (IOException e) {
                System.err.println("Problem z wczytywaniem odpowiedzi użytkownika");
                System.exit(-1);
                return null;
        }
}

    public void showMsg(String msg) {
            System.out.println(msg);
    }
    
    /**
     * {@inheritDoc}
     */     
    public void showErrorMsg(String msg) {
            System.err.println("[BŁĄD] "+msg+"\n");
    }
    
    public boolean doOneAction() {
        
    	ManagePublishAction action = null;
        do {
        	if (action == ManagePublishAction.INVALID) {
        		showGreeting();
        	}
                action = showMenuAndGetAction();
        } while (action == ManagePublishAction.INVALID);
        
        switch (action) {
        case PUBLISH_SHOWALL:
        	showAllPublish();
        	showGreeting();
        	break;
        case PUBLISH_ADD :
        	addPublish();
        	showGreeting();
                break;
        case PUBLISH_EDIT :
        	editPublish();
        	showGreeting();
                break;
        case PUBLISH_SEARCH : 
        	searchPublish();
        	showGreeting();
        		break;
        case PUBLISH_DELETE :
        	deletePublish();
        	showGreeting();
                break;
        case BACK : 
        	MainMenu.instance().go();
        		break;
        default: return false;
        }
        
        return true;
    }
    
    private void showAllPublish() {
    	System.out.println("Wydawnictwa zapisane w bazie: ");
    	System.out.println("ID | Nazwa");
    	
    	wm.listWydawnictwa();
    }
    private void addPublish() {
    	System.out.println("Podaj nazwe wydawnictwa: ");
    	String wydName = getUserInput();
    	
    	wm.addWydawnictwo(wydName);
    	
    }
    
    private void editPublish() {
    	
    	
    	try {
    		System.out.println("Podaj ID wydawnictwa do zmiany:");
    		int wydId = Integer.valueOf(getUserInput());
    		
    	    System.out.println("Podaj nowa nazwę wydawnictwa: ");
    	    String newWydName = getUserInput();
    	    	
    	    wm.updateWydawnictwo(wydId, newWydName);
        } catch (NumberFormatException e) {
    		System.err.println("BŁĄD: ID wydawnictwa musi być liczbą całkowitą.");
    	}    	
    }
    
    private void searchPublish() {
    	System.out.println("Podaj nazwę wydawnictwa do wyszukania:");
    	
    	String searchWydName = getUserInput();
    	
    	wm.searchWydawnictwoWithoutList(searchWydName);
    }
    
    private void deletePublish() {
    	try {
    		System.out.println("Podaj ID wydawnictwa do usunięcia z bazy:");
    		int wydId = Integer.valueOf(getUserInput());
    		
    		wm.deleteWydawnictwo(wydId);
    	} catch (NumberFormatException e) {
    		System.err.println("BŁĄD: ID wydawnictwa musi być liczbą całkowitą.");
    	}
    	
    }

}
