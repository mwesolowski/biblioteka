package pl.biblioteka.ui.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import pl.biblioteka.ui.UserInterface;
import pl.biblioteka.ui.action.MainMenuAction;

public class MainMenu implements UserInterface {
	
	private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	private static volatile MainMenu instance;
	
	private MainMenu() {
		
	}
	
	public static MainMenu instance() {
		if (instance == null) 
			instance = new MainMenu();
		
		return instance;
	}

	public void showGreeting() {
		System.out.println("+------------------------------+\n"+
                "|           Biblioteka         |\n"+
                "+------------------------------+");
		
	}
	
	public void go() {
		showGreeting();
		while (doOneAction()) {}
	}

	public MainMenuAction showMenuAndGetAction() {
		System.out.println("\nCo checsz zrobić?\n" +
                "1. Zarządzaj czytelnikami.\n" +
				"2. Zarządzaj autorami.\n" + 
				"3. Zarządzaj księgozbiorem.\n" +
                "4. Zarządzaj bazą wydawnictw.\n" + 
                "5. Zarządzaj wypożyczeniami.\n" +
				"6. Zakończ program.\n" +
                "(wpisz cyfrę od 1 do 6)");
		String choice = getUserInput();
		return MainMenuAction.parse(choice);
	}
	
	private String getUserInput() {
        try {
                return br.readLine();
        } catch (IOException e) {
                System.err.println("Problem z wczytywaniem odpowiedzi użytkownika");
                System.exit(-1);
                return null;
        }
}

    public void showMsg(String msg) {
            System.out.println(msg);
    }
    
    /**
     * {@inheritDoc}
     */     
    public void showErrorMsg(String msg) {
            System.err.println("[BŁĄD] "+msg+"\n");
    }
    
    public boolean doOneAction() {
        
        MainMenuAction action;
        do {
                action = showMenuAndGetAction();
        } while (action == MainMenuAction.INVALID);
        
        switch (action) {
        case MANAGE_READERS :
        	ManageReadersMenu.instance().go();
                break;
        case MANAGE_AUTHORS:
        	ManageAuthorsMenu.instance().go();
        	break;
        case MANAGE_BOOKS :
        	ManageBooksMenu.instance().go();
                break;
        case MANAGE_PUBLISH :
        	ManagePublishMenu.instance().go();
        		break;
        case MANAGE_RENTS :
                break;
        case QUIT :
                System.exit(0);
                return false;
        }
        
        return true;
    }

	
}
