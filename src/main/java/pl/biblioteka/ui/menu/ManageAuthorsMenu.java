package pl.biblioteka.ui.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import pl.biblioteka.entityManagers.AutorManager;
import pl.biblioteka.ui.action.ManageAuthorsAction;

public class ManageAuthorsMenu {
	
	private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	private AutorManager am = AutorManager.instance();
	
	private static volatile ManageAuthorsMenu instance;
	
	private ManageAuthorsMenu() {
		
	}
	
	public static ManageAuthorsMenu instance() {
		if (instance == null) {
			instance = new ManageAuthorsMenu();
		}
		
		return instance;
	}
	

	public void showGreeting() {
		System.out.println(
				"+---------------------------------------------+\n"+
                "|            Zarządzanie autorami             |\n"+
                "+---------------------------------------------+");
		
	}
	
	public void go() {
		showGreeting();
		while (doOneAction()) {}
	}

	public ManageAuthorsAction showMenuAndGetAction() {
		System.out.println("\nCo checsz zrobić?\n" +
				"1. Wyświetlić wszystkich autorów istniejących w bazie.\n" +
                "2. Dodać nowego autora.\n" + 
				"3. Zmienić dane autora.\n" + 
                "4. Wyszukać w bazie autora.\n" + 
				"5. Usunąć autora z bazy.\n" +
                "6. Wrócić do poprzedniego menu.\n" +
                "(wpisz cyfrę od 1 do 6)");
		String choice = getUserInput();
		return ManageAuthorsAction.parse(choice);
	}
	
	private String getUserInput() {
        try {
                return br.readLine();
        } catch (IOException e) {
                System.err.println("Problem z wczytywaniem odpowiedzi użytkownika");
                System.exit(-1);
                return null;
        }
}

    public void showMsg(String msg) {
            System.out.println(msg);
    }
    
    /**
     * {@inheritDoc}
     */     
    public void showErrorMsg(String msg) {
            System.err.println("[BŁĄD] "+msg+"\n");
    }
    
    public boolean doOneAction() {
        
    	ManageAuthorsAction action = null;
        do {
        	if (action == ManageAuthorsAction.INVALID) {
        		showGreeting();
        	}
                action = showMenuAndGetAction();
        } while (action == ManageAuthorsAction.INVALID);
        
        switch (action) {
        case AUTHOR_SHOWALL:
        	showAllAuthors();
        	showGreeting();
        	break;
        case AUTHOR_ADD :
        	addAuthor();
        	showGreeting();
                break;
        case AUTHOR_EDIT :
        	editAuthor();
        	showGreeting();
                break;
        case AUTHOR_SEARCH : 
        	searchAuthor();
        	showGreeting();
        		break;
        case AUTHOR_DELETE :
        	deleteAuthor();
        	showGreeting();
                break;
        case BACK : 
        	MainMenu.instance().go();
        		break;
        default: return false;
        }
        
        return true;
    }
    
    private void showAllAuthors() {
    	
    	
    	am.listAuthors();

    }
    private void addAuthor() {
    	System.out.println("Podaj nazwe autora: ");
    	String autName = getUserInput();
    	
    	am.addAutor(autName);
    	
    }
    
    private void editAuthor() {
       	try {
    		System.out.println("Podaj ID autora do zmiany:");
    		int autID = Integer.valueOf(getUserInput());
    		
   	    	System.out.println("Podaj nową nazwę autora:");
   	    	String newAutName = getUserInput();
   	    	
   	    	am.updateAutor(autID, newAutName);
    		
    	} catch (NumberFormatException e) {
    		System.out.println("BŁĄD: ID autora musi być liczbą całkowitą.");
    	}	
    }
    
    private void searchAuthor() {
    	System.out.println("Podaj nazwę autora do wyszukania:");
    	String searchAutName = getUserInput();
    
    	am.searchAutorWithoutList(searchAutName);
    	
    }
    
    private void deleteAuthor() {
    	
    	try {
    		System.out.println("Podaj ID autora do usunięcia z bazy:");
    		int autID = Integer.valueOf(getUserInput());
        	
        	am.deleteAutor(autID);
    		
    	} catch (NumberFormatException e) {
    		System.err.println("BŁĄD: ID wydawnictwa jest liczbą całkowitą.");
    	}
    		
    }

}
