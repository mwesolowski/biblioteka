package pl.biblioteka.ui.menu; 

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import pl.biblioteka.entities.Autor;
import pl.biblioteka.entities.Ksiazka;
import pl.biblioteka.entities.Wydawnictwo;
import pl.biblioteka.entityManagers.AutorManager;
import pl.biblioteka.entityManagers.KsiazkaManager;
import pl.biblioteka.entityManagers.WydawnictwoManager;
import pl.biblioteka.ui.UserInterface;
import pl.biblioteka.ui.action.ManageBooksAction;

public class ManageBooksMenu implements UserInterface {

	private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	private static volatile ManageBooksMenu instance;
	
	private ManageBooksMenu() {
		
	}
	
	public static ManageBooksMenu instance() {
		if (instance == null) {
			instance = new ManageBooksMenu();
		}
		
		return instance;
	}
	

	public void showGreeting() {
		System.out.println("" +
				"+--------------------------------------------+\n"+
                "|           Zarządzanie księgozbiorem        |\n"+
                "+--------------------------------------------+");
		
	}
	
	public void go() {
		showGreeting();
		while (doOneAction()) {}
	}

	public ManageBooksAction showMenuAndGetAction() {
		System.out.println("\nCo checsz zrobić?\n" +
                "1. Wyświetl wszystkie książki w zbiorze.\n" +
				"2. Zmienić dane książki.\n" +
                "3. Dodać nową książkę. \n" + 
                "4. Usunąć książkę.\n" +
				"5. Wyszukać książkę po tytule.\n" +
                "6. Wyszukać ksiązkę po ISBN.\n" +
				"7. Powrócić do poprzedniego menu.\n" +
                "(wpisz cyfrę od 1 do 7)");
		String choice = getUserInput();
		return ManageBooksAction.parse(choice);
	}
	
	private String getUserInput() {
        try {
                return br.readLine();
        } catch (IOException e) {
                System.err.println("Problem z wczytywaniem odpowiedzi użytkownika");
                System.exit(-1);
                return null;
        }
}

    public void showMsg(String msg) {
            System.out.println(msg);
    }
    
    /**
     * {@inheritDoc}
     */     
    public void showErrorMsg(String msg) {
            System.err.println("[BŁĄD] "+msg+"\n");
    }
    
    public boolean doOneAction() {
        
    	ManageBooksAction action = null;
        do {
        	if (action == ManageBooksAction.INVALID){
        		showGreeting();
        	}
                action = showMenuAndGetAction();
        } while (action == ManageBooksAction.INVALID);
        
        switch (action) {
        case BOOKS_GETALL :
        	showAllBooks();
        	showGreeting();
        	    break;
        case BOOKS_EDIT :
        	editBook();
        	showGreeting();
                break;
        case BOOKS_ADD : 
        	dodajKsiazke();
        	showGreeting();
        		break;
        case BOOKS_DELETE :
        	deleteBook();
        	showGreeting();
                break;
        case BOOKS_SEARCH_BY_TITLE :
        	searchByTitle();
        	showGreeting();
        		break;
        case BOOKS_SEARCH_BY_ISBN :
        	searchByISBN();
        	showGreeting();
        	break;
        case PREVIOUS :
                MainMenu.instance().go();
        default: return false;
        }
        
        return true;
    }

	private void showAllBooks() {
		// TODO Auto-generated method stub
		KsiazkaManager.instance().showAllKsiazki();
	}
	
    
	private void dodajKsiazke() {
		try {
			System.out.println("Podaj ID autora z poniższych: ");
			AutorManager.instance().listAuthors();
			int autID = Integer.parseInt(getUserInput());
			
			System.out.println("Podaj datę wydania w formacie miesiąc/rok - wg wzoru: 02/1991");
			String dWydania = getUserInput();
			
			System.out.println("Podaj tytuł książki: ");
			String tKsiazki = getUserInput();
			
			System.out.println("Podaj ISBN książki: ");
			String isbn = getUserInput();
			
			System.out.println("Podaj ID wydawnictwa z poniższych: ");
			WydawnictwoManager.instance().listWydawnictwa();
			int wydID = Integer.parseInt(getUserInput());
			
			System.out.println("Podaj liczbę stron: ");
			int lStron = Integer.parseInt(getUserInput());
			
			KsiazkaManager.instance().addKsiazka(autID, dWydania, tKsiazki, isbn, wydID, lStron);
		} catch (NumberFormatException e) {
			System.err.println("BŁĄD: ID: autora, wydawnictwa, liczba stron są liczbami całkowitymi.");
		}
	}
	private void searchByTitle() {
		System.out.println("Podaj tytuł książki do wyszukania:" );
		KsiazkaManager.instance().getKsiazkaByTitle(getUserInput());
		
	}
	
	private void searchByISBN() {
		System.out.println("Podaj ISBN książki do wyszukania:");
		KsiazkaManager.instance().getKsiazka(getUserInput());
		
	}
	
	private void deleteBook() {
		try {
			System.out.println("Podaj ID książki do usunięcia: ");
			int idKsiazki = Integer.parseInt(getUserInput());
			
			KsiazkaManager.instance().deleteKsiazka(idKsiazki);
		} catch (NumberFormatException e) {
			System.err.println("BŁĄD: ID książki jest liczbą całkowitą.");
		}
	}
   
	private void editBook() {
		try {
			System.out.println("Podaj ID książki do zmiany: ");
			int ksID = Integer.parseInt(getUserInput());
			
			Ksiazka oldKs = KsiazkaManager.instance().getKsiazka(ksID);
			Ksiazka newKs = new Ksiazka();
			if (oldKs == null) {
				
			} else {			
				System.out.println("Podaj nowe ID autora z poniższych: ");
				AutorManager.instance().listAuthors();
				int autId = Integer.parseInt(getUserInput());
				Autor newAut = new Autor();
				newAut = AutorManager.instance().getAutor(autId);
				newKs.setAutor(newAut);
				
				System.out.println("Podaj nową datę wydania w formacie miesiąc/rok - wg wzoru: 02/1991");
				newKs.setDataWydania(KsiazkaManager.instance().parseDate(getUserInput()));
				
				System.out.println("Podaj nowy tytuł książki:  (lub pozostaw pustem aby zostawić starego)");
				newKs.setTytul(getUserInput().trim());
				
				System.out.println("Podaj nowy ISBN książki:  (lub pozostaw pustem aby zostawić starego)");
				newKs.setIsbn(getUserInput().trim());
				
				System.out.println("Podaj nowe ID wydawnictwa z poniższych:");
				WydawnictwoManager.instance().listWydawnictwa();
				int wydID = Integer.parseInt(getUserInput());
				Wydawnictwo newWyd = new Wydawnictwo();
				newWyd = WydawnictwoManager.instance().getWydawnictwo(wydID);
				newKs.setWydawnictwo(newWyd);
				
				System.out.println("Podaj nową liczbę stron:  (lub pozostaw pustem aby zostawić starego)");
				newKs.setLiczbaStron(Integer.parseInt(getUserInput())); 
				
				KsiazkaManager.instance().editKsiazka(ksID, newKs);
			}
			
		} catch (NumberFormatException e) {
			System.err.println("BŁĄD: Wystąpił bład podczas wprowadzania danych.");
		} 
	}
}
