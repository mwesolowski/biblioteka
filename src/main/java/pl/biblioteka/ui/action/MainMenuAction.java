package pl.biblioteka.ui.action;

public enum MainMenuAction {
	MANAGE_READERS,
	MANAGE_AUTHORS,
	MANAGE_BOOKS,
	MANAGE_PUBLISH,
    MANAGE_RENTS,
    QUIT,
    INVALID ;
    public static MainMenuAction parse(String str) {
            try {
                    int ret = Integer.valueOf(str);
                    switch(ret) {
                            case 1: return MANAGE_READERS;
                            case 2: return MANAGE_AUTHORS;
                            case 3: return MANAGE_BOOKS;
                            case 4: return MANAGE_PUBLISH;
                            case 5: return MANAGE_RENTS;
                            case 6: return QUIT;
                            default: return INVALID;
                    }
            } catch (NumberFormatException e) {
                    return INVALID;
            }
    }
}