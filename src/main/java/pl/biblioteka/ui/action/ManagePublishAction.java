package pl.biblioteka.ui.action;

public enum ManagePublishAction {
	PUBLISH_SHOWALL,
	PUBLISH_ADD, 
	PUBLISH_EDIT, 
	PUBLISH_SEARCH, 
	PUBLISH_DELETE, 
	BACK, 
	INVALID;
	public static ManagePublishAction parse(String str) {
		try {
			int ret = Integer.valueOf(str);
			switch (ret) {
			case 1:
				return PUBLISH_SHOWALL;
			case 2:
				return PUBLISH_ADD;
			case 3:
				return PUBLISH_EDIT;
			case 4:
				return PUBLISH_SEARCH;
			case 5:
				return PUBLISH_DELETE;
			case 6:
				return BACK;
			default:
				return INVALID;
			}
		} catch (NumberFormatException e) {
			return INVALID;
		}
	}
}
	

