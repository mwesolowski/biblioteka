package pl.biblioteka.ui.action;

public enum ManageReadersAction {
	READERS_GETALL,
	READERS_EDIT,
	READERS_ADD,
    READERS_DELETE,
    READERS_SEARCH_BY_PESEL,
    READERS_SEARCH_BY_NAME,
    PREVIOUS,
    INVALID ;
    public static ManageReadersAction parse(String str) {
            try {
                    int ret = Integer.valueOf(str);
                    switch(ret) {
                            case 1: return READERS_GETALL;
                            case 2: return READERS_EDIT;
                            case 3: return READERS_ADD;
                            case 4: return READERS_DELETE;
                            case 5: return READERS_SEARCH_BY_PESEL;
                            case 6: return READERS_SEARCH_BY_NAME;
                            case 7: return PREVIOUS;
                            default: return INVALID;
                    }
            } catch (NumberFormatException e) {
                    return INVALID;
            }
    }
}
