package pl.biblioteka.ui.action;
 
public enum ManageBooksAction {
	BOOKS_GETALL,
	BOOKS_EDIT,
	BOOKS_ADD,
    BOOKS_DELETE,
    BOOKS_SEARCH_BY_TITLE,
    BOOKS_SEARCH_BY_ISBN,
    PREVIOUS,
    INVALID ;
    public static ManageBooksAction parse(String str) {
            try {
                    int ret = Integer.valueOf(str);
                    switch(ret) {
                            case 1: return BOOKS_GETALL;
                            case 2: return BOOKS_EDIT;
                            case 3: return BOOKS_ADD;
                            case 4: return BOOKS_DELETE;
                            case 5: return BOOKS_SEARCH_BY_TITLE;
                            case 6: return BOOKS_SEARCH_BY_ISBN;
                            case 7: return PREVIOUS;
                            default: return INVALID;
                    }
            } catch (NumberFormatException e) {
                    return INVALID;
            }
    }
}
