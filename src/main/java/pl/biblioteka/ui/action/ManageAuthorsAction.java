package pl.biblioteka.ui.action;

public enum ManageAuthorsAction {
	AUTHOR_SHOWALL,
	AUTHOR_ADD, 
	AUTHOR_EDIT, 
	AUTHOR_SEARCH, 
	AUTHOR_DELETE, 
	BACK, 
	INVALID;
	public static ManageAuthorsAction parse(String str) {
		try {
			int ret = Integer.valueOf(str);
			switch (ret) {
			case 1:
				return AUTHOR_SHOWALL;
			case 2:
				return AUTHOR_ADD;
			case 3:
				return AUTHOR_EDIT;
			case 4:
				return AUTHOR_SEARCH;
			case 5:
				return AUTHOR_DELETE;
			case 6:
				return BACK;
			default:
				return INVALID;
			}
		} catch (NumberFormatException e) {
			return INVALID;
		}
	}
}
	
