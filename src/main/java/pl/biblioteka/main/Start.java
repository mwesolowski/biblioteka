package pl.biblioteka.main;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import pl.biblioteka.entities.Autor;
import pl.biblioteka.entities.Ksiazka;
import pl.biblioteka.entityManagers.AutorManager;
import pl.biblioteka.entityManagers.KsiazkaManager;
import pl.biblioteka.ui.menu.MainMenu;

public class Start {

	private static SessionFactory factory;
	private static ServiceRegistry serviceRegistry;
	
    public static void main(String[] args){
    	
    		try {
    			Configuration configuration = new Configuration();
        		configuration.configure();
			    		
        		serviceRegistry = new ServiceRegistryBuilder().applySettings(
        				configuration.getProperties()).buildServiceRegistry();
        		
        		setFactory(configuration.buildSessionFactory(serviceRegistry)); 
        		
    		} catch (Throwable e) {
    			System.err.println("Failed to create sessionFactory object." + e);
    			throw new ExceptionInInitializerError(e);
    		}
          new Start().go();      
    }
    

    /**
     * Przygotowania i główna pętla programu.
     */
    private void go() {
            MainMenu.instance().go();
    }
   
    public static SessionFactory getFactory() {
		return factory;
	}

	public static void setFactory(SessionFactory factory) {
		Start.factory = factory;
	}


}
