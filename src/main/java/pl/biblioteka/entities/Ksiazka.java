package pl.biblioteka.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table (name="ksiazki")
public class Ksiazka {
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy="increment")
	@Column (name = "id", unique = true, nullable = false)
	private int id;
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn(name="id_autor", nullable = false)
	private Autor autor;
	
	@Column (name = "data_wydania", nullable=false)
	private Date dataWydania;
	
	@Column (name = "tytul", nullable=false, length = 255)
	private String tytul;
	
	@Column (name = "isbn", nullable=false, length=13)
	private String isbn;
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn(name="id_wydawnictwo", nullable = false)
	private Wydawnictwo wydawnictwo;
	
	@Column (name = "liczba_stron", nullable=false)
	private int liczbaStron;
	
	@Column (name = "czy_wyp", nullable=false)
	private boolean czyWypozyczona;
	
	public String getTytul() {
		return tytul;
	}
	public void setTytul(String tytul) {
		this.tytul = tytul;
	}
	
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String string) {
		this.isbn = string;
	}
	
	public int getLiczbaStron() {
		return liczbaStron;
	}
	public void setLiczbaStron(int liczbaStron) {
		this.liczbaStron = liczbaStron;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Autor getAutor() {
		return autor;
	}
	public void setAutor(Autor autor) {
		this.autor = autor;
	}
	
	public Date getDataWydania() {
		return dataWydania;
	}
	public void setDataWydania(Date dataWydania) {
		this.dataWydania = dataWydania;
	}
	public Wydawnictwo getWydawnictwo() {
		return wydawnictwo;
	}
	public void setWydawnictwo(Wydawnictwo wydawnictwo) {
		this.wydawnictwo = wydawnictwo;
	}
	public boolean isCzyWypozyczona() {
		return czyWypozyczona;
	}
	public void setCzyWypozyczona(boolean czyWypozyczona) {
		this.czyWypozyczona = czyWypozyczona;
	}
	
	public String toString() {
		return id + "| " + autor.getNazwa() + "| " + tytul + "| " + isbn + "| " + wydawnictwo.getNazwa() + "| " + liczbaStron + "| " + czyWypozyczona;
	}
	
	
	
}
