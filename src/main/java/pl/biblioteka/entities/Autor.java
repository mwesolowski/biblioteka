package pl.biblioteka.entities;

import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table (name="autorzy", uniqueConstraints = {
						@UniqueConstraint(columnNames = "nazwa")})

public class Autor{
	
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy="increment")
	@Column (name = "id", unique=true, nullable = false)
	private int id;
	
	@Column (name = "nazwa", unique=true, nullable = false, length = 255)
	private String nazwa;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "autor")
	private Set<Ksiazka> ksiazki;
	
	public Autor(String nazwa) {
		this.nazwa=nazwa;
	}
	
	public Autor(int id) {
		this.id = id;
	}
	
	public Autor() {		
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Set<Ksiazka> getKsiazki() {
		return ksiazki;
	}

	public void setKsiazki(Set<Ksiazka> ksiazki) {
		this.ksiazki = ksiazki;
	}
	
	
}
