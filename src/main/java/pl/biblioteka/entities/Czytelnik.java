package pl.biblioteka.entities;

import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table (name="czytelnicy", uniqueConstraints = { 
						   @UniqueConstraint(columnNames="pesel") })
public class Czytelnik {
	
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy="increment")
	@Column (name = "id", unique=true, nullable=false)
	private int id;
	
	@Column (name="imie", nullable = false, length = 255)
	private String imie;
	
	@Column (name="nazwisko", nullable = false, length = 255)
	private String nazwisko;
	
	@Column (name="email", nullable=false, length=255)
	private String email;
	
	@Column (name="adresUlica", nullable=false, length=255)
	private String adresUlica;
	
	@Column (name="adresNrDomu", nullable=false, length=255)
	private String adresNrDomu;
	
	@Column (name="adresNrMieszkania", nullable=true, length=10)
	private String adresNrMieszkania;
	
	@Column (name="miasto", nullable=false, length=30)
	private String adresMiasto;
	
	@Column (name="pesel", nullable=false, length=11)
	private String pesel;
	
	public Czytelnik() {
		
	}
	public Czytelnik(String im, String nazw, String pesel2, String email2, String adresUlica2, String adresNrDomu2, String adresMiasto2, String adresNrMieszkania2) {
		// TODO Auto-generated constructor stub
		this.imie=im;
		this.nazwisko=nazw;
		this.pesel=pesel2;
		this.email=email2;
		this.adresUlica=adresUlica2;
		this.adresNrDomu=adresNrDomu2;
		this.adresMiasto=adresMiasto2;
		this.adresNrMieszkania=adresNrMieszkania2;
		
	}
	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAdresUlica() {
		return adresUlica;
	}
	public void setAdresUlica(String adresUlica) {
		this.adresUlica = adresUlica;
	}
	public String getAdresNrDomu() {
		return adresNrDomu;
	}
	public void setAdresNrDomu(String adresNrDomu) {
		this.adresNrDomu = adresNrDomu;
	}
	public String getAdresNrMieszkania() {
		return adresNrMieszkania;
	}
	public void setAdresNrMieszkania(String adresNrMieszkania) {
		this.adresNrMieszkania = adresNrMieszkania;
	}

	public String getAdresMiasto() {
		return adresMiasto;
	}

	public void setAdresMiasto(String adresMiasto) {
		this.adresMiasto = adresMiasto;
	}
	
	
	public String toString() {
		// TODO Auto-generated method stub
		return id + "| " + nazwisko + "| " + pesel + "| " + email + "| " + adresUlica + 
				"| " + adresNrDomu + "| " + adresNrMieszkania + "| " + adresMiasto;
	}
	
}
