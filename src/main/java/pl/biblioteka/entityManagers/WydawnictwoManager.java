package pl.biblioteka.entityManagers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import pl.biblioteka.entities.Wydawnictwo;
import pl.biblioteka.main.Start;

public class WydawnictwoManager {
	
	private Session session;
	private Transaction tx;
	private SessionFactory factory = Start.getFactory();
	private Query query;
	
	private static WydawnictwoManager instance;
	
	public static WydawnictwoManager instance() {
		if (instance == null) 
			instance = new WydawnictwoManager();
		
		return instance;
	}
	
	
	private WydawnictwoManager() {
	}
		
	// Dodawanie wydawnictwa do bazy
	public int addWydawnictwo(String wydName) {
		if (wydName.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano nazwy wydawnictwa.");
			return -1;
		} else {
			if (wydName.trim().length() > 255) {
				System.err.println("BŁĄD: Nazwa wydawnictwa nie może przekraczać 255 znaków.");
				return -1;
			} else {
				List<Wydawnictwo> wydList = searchWydawnictwo(wydName);

				if (wydList.size() > 0) {
					for (Wydawnictwo out : wydList) {
						if (out.getNazwa().trim().equals(wydName.trim())) {
							System.err.println("BŁĄD: Wydawnictwo o podanej nazwie istnieje w bazie.");
							return -1;
						}
					}
				}
			}
		}
		session = factory.openSession();
		tx = null;
		Integer wydID = null;
		
		try {
			tx = session.beginTransaction();
			Wydawnictwo wyd = new Wydawnictwo(wydName.trim());
			wydID = (Integer) session.save(wyd);
			tx.commit();
			System.out.println("Wydawnictwo o nazwie " + wydName + " dodane do bazy danych");
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return wydID;
	}
	
	// Wyswietlanie wszystkich wydawnictw zapisanych w bazie
	public void listWydawnictwa() {
		session = factory.openSession();
		tx = null;
		
		try {
			tx = session.beginTransaction();
			List wydawnictwa = session.createQuery("FROM Wydawnictwo").list();
			
			for (Iterator iterator = wydawnictwa.iterator(); iterator.hasNext();) {
				Wydawnictwo wydawnictwo = (Wydawnictwo) iterator.next();
				System.out.print(wydawnictwo.getId());
				System.out.println("| " + wydawnictwo.getNazwa());
			}
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	// Zmiana nazwy wydawnictwa
	public boolean updateWydawnictwo(int wydID, String nazwa) {
		if (wydID < 0) {
			System.err.println("BŁĄD: Podano nieprawidłowy nr ID wydawnictwa do zmiany nazwy. ID wydawnictwa jest liczbą całkowitą dodatnią.");
			return false;
		}
		if (nazwa.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano nowej nazwy wydawnictwa.");
			return false;
		} else {
			if (nazwa.trim().length() > 255) {
				System.err.println("BŁĄD: Nowa nazwa wydawnictwa nie może przekraczać 255 znaków.");
				return false;
			}
		}
		
		session = factory.openSession();
		tx = null;
		
		try {
			tx = session.beginTransaction();
			Wydawnictwo wydawnictwo = (Wydawnictwo) session.get(Wydawnictwo.class, wydID);
			
			if (wydawnictwo == null) {
				System.err.println("BŁĄD: Wydawnictwo o numerze ID " + wydID + " nie istnieje w bazie.");
				throw new HibernateException("");
			}

			if (wydawnictwo.getNazwa().trim().equals(nazwa.trim()) ) {
				System.err.println("BŁĄD: Nowa nazwa wydawnictwa jest taka sama jak stara nazwa.");
				throw new HibernateException("");
			}
			wydawnictwo.setNazwa(nazwa.trim());
					
			session.update(wydawnictwo);
			tx.commit();
			
			System.out.println("Nazwa wydawnictwa zmieniona.");
			return true;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			
			return false;
		} finally {
			session.close();
		}
	}
	
	// kasowanie wydawnictwa
	public boolean deleteWydawnictwo(int wydID) {
		
		if (wydID < 0) {
			System.err.println("BŁĄD: Podano nieprawidłowy nr ID wydawnictwa do zmiany nazwy. ID wydawnictwa jest liczbą całkowitą dodatnią.");
			return false;
		}
		
		session = factory.openSession();
		tx = null;
		
		try {
			tx = session.beginTransaction();
			Wydawnictwo wydawnictwo = (Wydawnictwo) session.get(Wydawnictwo.class, wydID);
						
			if ( wydawnictwo == null) {
				System.err.println("BŁĄD: Wydawnictwo o numerze ID " + wydID + " nie istnieje w bazie.");
				throw new HibernateException("");
			}
			session.delete(wydawnictwo);
			tx.commit();
			return true;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			
			return false;
		} finally {
			session.close();
		}
	}
	
	// wyszukiwanie wydawnictwa po nazwie - zwraca liste dla addWydawnictwo
	private List<Wydawnictwo> searchWydawnictwo(String wydName) {
		List<Wydawnictwo> wydawnictwa = new ArrayList<Wydawnictwo>();
		if (wydName.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano nazwy wydawnictwa do wyszukania.");
			return wydawnictwa;
		} else {
			if (wydName.trim().length() > 255) {
				System.err.println("BŁĄD: Nazwa wydawnictwa nie może przekraczać 255 znaków.");
				return wydawnictwa;
			}
		}
		session = factory.openSession();
		tx = null;
		
		try {
			tx = session.beginTransaction();
			query = session.createQuery("FROM Wydawnictwo WHERE UPPER(nazwa) like UPPER(:wydName)");
			wydawnictwa = query.setParameter("wydName", "%" + wydName.trim() + "%").list();
			tx.commit();
			return wydawnictwa;
		} catch (HibernateException e) {
			if (tx != null) 
				tx.rollback();			
			e.printStackTrace();
			return wydawnictwa;
		} finally {
			session.close();
		}		
	}
	
	// zwykle przeszukiwanie wydawnictwa bez zwracania listy
	public void searchWydawnictwoWithoutList(String wydName) {
		if (wydName.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano nazwy wydawnictwa do wyszukania.");
			return;
		} else {
			if (wydName.trim().length() > 255) {
				System.err.println("BŁĄD: Nazwa wydawnictwa nie może przekraczać 255 znaków.");
				return;
			}
		}
		session = factory.openSession();
		tx = null;
		
		List<Wydawnictwo> wydawnictwa = new ArrayList<Wydawnictwo>();
		
		try {
			tx = session.beginTransaction();
			query = session.createQuery("FROM Wydawnictwo WHERE UPPER(nazwa) like UPPER(:wydName)");
			wydawnictwa = query.setParameter("wydName", "%" + wydName.trim() + "%").list();
			
			tx.commit();
			
			if (wydawnictwa.size() == 0) {
				System.out.println("Nie znaleziono wydawnictwa o podanej nazwie.");
			} else {
				System.out.println("Wyniki wyszukiwania:");
				System.out.println("ID| Nazwa");
				for (Iterator iterator=wydawnictwa.iterator(); iterator.hasNext();) {
					Wydawnictwo wyd = (Wydawnictwo) iterator.next();
					System.out.print(wyd.getId());
					System.out.println("| " + wyd.getNazwa());
				}
			}
		} catch (HibernateException e) {
			if (tx != null) 
				tx.rollback();			
			e.printStackTrace();
		} finally {
			session.close();
		}		
	}
	
	// wyciaganie wydawnictwa o podanym ID
	public Wydawnictwo getWydawnictwo(int wydID) {
		Wydawnictwo wydawnictwo = new Wydawnictwo();
		session = factory.openSession();
		tx = null;
		
		try {
			tx = session.beginTransaction();
			wydawnictwo = (Wydawnictwo) session.get(Wydawnictwo.class, wydID);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return wydawnictwo;
	}
}
