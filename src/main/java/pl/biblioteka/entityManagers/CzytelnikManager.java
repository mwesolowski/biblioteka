package pl.biblioteka.entityManagers;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.biblioteka.entities.Czytelnik;
import pl.biblioteka.main.Start;

public class CzytelnikManager {

	private Session session;
	private Transaction tx;
	private SessionFactory factory = Start.getFactory();
	private Query query;

	private static CzytelnikManager instance;

	public static CzytelnikManager instance() {
		if (instance == null)
			instance = new CzytelnikManager();

		return instance;
	}

	private CzytelnikManager() {
	}

	public boolean addCzytelnik(String im, String nazw, String pesel,
			String email, String adresUlica, String adresNrDomu,
			String adresNrMieszkania, String adresMiasto) {

		// sprawdzam czy wypelniono wszystkie pola z wyjatkiem numeru mieszkania
		// - jest on opcjonalny
		if (im.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano imienia!");
			return false;
		}
		if (nazw.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano nazwiska!");
			return false;
		}
		if (pesel.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano numeru PESEL!");
			return false;
		} else {
			if (pesel.trim().length() > 11) {
				System.out
						.println("BŁĄD: Zbyt długi PESEL - numer powinien zawierać 11 cyfr.");
				return false;
			} else {
				if (pesel.trim().length() < 11) {
					System.out
							.println("BŁĄD: Zbyt krótki numer PESEL - numer powinien zawierąć 11 cyfr.");
					return false;
				} else {
					// sprawdzaj czy pesel jest poprawny
				}
			}
		}

		if (email.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano adresu email!");
			return false;
		}
		if (adresUlica.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano ulicy!");
			return false;
		}
		if (adresNrDomu.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano numeru domu!");
			return false;
		}
		if (adresMiasto.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano nazwy miasta!");
			return false;
		}

		// sprawdzamy tylko czy czytelnik o takim samym peselu istnieje w bazie
		if (findCzytelnik(pesel.trim()) != null) {
			System.err
					.println("BŁĄD: Czytelnik o podanym numerze PESEL już istnieje w bazie.");
			return false;
		} else {
			// tu jeszcze walidacja poprawnosci peselu
			session = factory.openSession();
			tx = null;
			Integer czytID = null;
			try {
				tx = session.beginTransaction();
				Czytelnik czyt = new Czytelnik(im.trim(), nazw.trim(),
						pesel.trim(), email.trim(), adresUlica.trim(),
						adresNrDomu.trim(), adresMiasto.trim(),
						adresNrMieszkania.trim());
				System.out.println(czyt.getAdresMiasto());
				czytID = (Integer) session.save(czyt);
				tx.commit();
				System.out.println("Czytelnik dodany do bazy danych.");

			} catch (HibernateException e) {
				if (tx != null)
					tx.rollback();

				e.printStackTrace();
			} finally {
				session.close();
			}
			// }
			return false;
		}
	}

	public Czytelnik findCzytelnik(String pesel) {
		Czytelnik czytelnik = new Czytelnik();
		if (pesel.trim().length() == 0) {
			System.out
					.println("BŁĄD: Nie podano numeru PESEL do wyszukania czytelnika.");
			return czytelnik;
		} else {
			if (pesel.trim().length() < 11) {
				System.out
						.println("BŁĄD: Numer PESEL powinien zawierać 11 cyfr.");
				return new Czytelnik();
			} else {
				session = factory.openSession();
				tx = null;

				try {
					tx = session.beginTransaction();

					query = session.createQuery(
							"from Czytelnik where pesel=:pesel").setParameter(
							"pesel", pesel);
					czytelnik = (Czytelnik) query.uniqueResult();

					tx.commit();
					
					if (czytelnik == null) {
						System.out.println("Nie znaleziono czytelnika o podanym PESEL.");
					} else {
						System.out.println("Znaleziono czytelnika o podanym PESEL. Oto jego dane:");
						System.out.println(czytelnik);
					}

				} catch (HibernateException e) {
					if (tx != null)
						tx.rollback();
					e.printStackTrace();
				} finally {
					session.close();
				}

			}
		}

		return czytelnik;
	}

	public Czytelnik findCzytelnik(int idCzyt) {
		Czytelnik czytelnik = new Czytelnik();
		if (idCzyt < 0) {
			System.out.println("BŁĄD: Podano nieprawodłowy nr ID czytelnika.");
			return czytelnik;
		} else {
			session = factory.openSession();
			tx = null;
			try {
				tx = session.beginTransaction();
				czytelnik = (Czytelnik) session.get(Czytelnik.class, idCzyt);

				if (czytelnik == null) {
					System.err.println("BŁĄD: Czytelnik o numerze ID " + idCzyt
							+ " nie istnieje w bazie.");
					throw new HibernateException("");
				}
				tx.commit();

			} catch (HibernateException e) {
				if (tx != null)
					tx.rollback();
			} finally {
				session.close();
			}

		}
		return czytelnik;
	}

	public void findCzytelnikByNazwisko(String nazwisko) {
		List<Czytelnik> czList = new ArrayList<Czytelnik>();
		if (nazwisko.trim().length() == 0) {
			System.err
					.println("BŁĄD: Nie podano nazwiska czytelnika do wyszukania.");
		} else {
			if (nazwisko.trim().length() > 255) {
				System.err
						.println("BŁĄD: Nazwisko nie może przekraczać 255 znaków.");
			} else {
				session = factory.openSession();
				tx = null;
				try {
					tx = session.beginTransaction();
					query = session
							.createQuery("from Czytelnik where upper(nazwisko) like upper(:nazwisko)");
					czList = query.setParameter("nazwisko",
							nazwisko.trim() + "%").list();

					tx.commit();

					if (czList.size() == 0) {
						System.out
								.println("Nie znaleziono czytelnika o podanym nazwisku.");
					} else {
						System.out.println("Wyniki wyszukiwania:");
						System.out
								.println("ID| Imię| Nazwisko| PESEL| Email| Ulica| Nr domu| Nr mieszkania| Miasto");
						for (Iterator it = czList.iterator(); it.hasNext();) {
							Czytelnik czyt = (Czytelnik) it.next();
							System.out.println(czyt);
						}
					}

				} catch (HibernateException e) {
					if (tx != null)
						tx.rollback();
					e.printStackTrace();
				} finally {
					session.close();
				}
			}
		}

	}

	public boolean deleteCzytelnik(int czytelnikId) {
		boolean stan = false;

		if (czytelnikId < 0) {
			System.err.println("BŁĄD: Podano nieprawidłowy ID czytelnika do usunięcia.");
			return stan;
		} else {
			session = factory.openSession();
			tx = null;
			try {
				tx = session.beginTransaction();
				Czytelnik cz = (Czytelnik) session.get(Czytelnik.class,
						czytelnikId);

				if (cz == null) {
					System.err.println("BŁĄD: Czytelnik o numerze ID "
							+ czytelnikId + " nie istnieje w bazie.");
					throw new HibernateException("");
				}
				session.delete(cz);
				tx.commit();
				System.out.println("Pomyślnie usunięto czytelnika o ID: "
						+ czytelnikId);
				stan = true;

			} catch (HibernateException e) {
				if (tx != null)
					tx.rollback();

				stan = false;
			} finally {
				session.close();
			}
		}
		return stan;
	}

	public boolean updateCzytelnik(int czytID, Czytelnik newCz) {
		boolean stan = false;
		if (czytID < 0) {
			System.err.println("BŁĄD: Nieprawidłowy ID czytelnika do aktualizacji danych.");
			return stan;
		} else {

				session = factory.openSession();
				tx = null;
				try {
					tx = session.beginTransaction();
					Czytelnik c = (Czytelnik) session.get(Czytelnik.class,
							czytID);

					if (c == null) {
						System.err.println("BŁĄD: Czytelnik o numerze ID "
								+ czytID + " nie istnieje w bazie.");
						throw new HibernateException("");
					}

					// imie
					if (c.getImie().trim().equals(newCz.getImie().trim())) {
						System.out.println("Imię niezmienione - podano takie samo.");
					} else {
						if (newCz.getImie().trim().length() == 0) {
							System.out.println("Imię niezmienione - nie wpisano nowego.");
						} else {
							if (newCz.getImie().trim().length() > 255) {
								System.err.println("BŁĄD: Imię zbyt długie (przekracza 255 znaków)");
								throw new HibernateException("");
							} else {
								System.out.println("Nowe imię: " + newCz.getImie());
								c.setImie(newCz.getImie().trim());
							}
						}
					}
					
					// nazwisko
					if (c.getNazwisko().trim()
							.equals(newCz.getNazwisko().trim())) {
						System.out.println("Nazwisko niezmienone - podano takie samo.");
					} else {
						if (newCz.getNazwisko().trim().length() == 0) {
							System.out.println("Nazwisko niezmienione - nie wpisano nowego.");
						} else {
							if (newCz.getNazwisko().trim().length() > 255) {
								System.err.println("BŁĄD: Nazwisko zbyt długie (przekrzacza 255 znaków).");
								throw new HibernateException("");
							} else {
								System.out.println("Nowe nazwisko: " + newCz.getNazwisko());
								c.setNazwisko(newCz.getNazwisko().trim());
							}
						}
					}
					
					// pesel
					if (c.getPesel().trim().equals(newCz.getPesel().trim())) {
						System.out.println("PESEL niezmieniony - podano takie samo.");
					} else {
						if (newCz.getPesel().trim().length() == 0) {
							System.out.println("PESEL niezmieniony - nie wpisano nowego.");
						} else {
							if (newCz.getPesel().trim().length() != 11) {
								System.err.println("BŁĄD: PESEL musi zawierać 11 znaków.");
								throw new HibernateException("");
							} else {
								// sprawdzam czy jest to unikalny pesel
								if (findCzytelnik(newCz.getPesel().trim()) == null) {
									System.out.println("Nowy PESEL: " + newCz.getPesel());
									c.setPesel(newCz.getPesel().trim());
								} else {
									System.err.println("BŁĄD: Podany PESEL już istnieje w bazie.");
									throw new HibernateException("");
								}
							}

						}
					}
					
					// email
					if (c.getEmail().trim().equals(newCz.getEmail().trim())) {
						System.out.println("Email niezmieniony - podano taki sam.");
					} else {
						if (newCz.getEmail().trim().length() == 0) {
							System.out.println("Email niezmieniony - nie wpisano nowego.");
						} else {
							if (newCz.getEmail().trim().length() > 255) {
								System.err.println("BŁĄD: Email zbyt długi (przekracza 255 znaków.");
								throw new HibernateException("");
							} else {
								System.out.println("Nowy email: " + newCz.getEmail());
								c.setEmail(newCz.getEmail().trim());
							}
						}
					}
					
					// adresulica
					if (c.getAdresUlica().trim().equals(newCz.getAdresUlica().trim())) {
						System.out.println("Adres - Ulica niezmieniona - podano takią samą.");
					} else {
						if (newCz.getAdresUlica().trim().length() == 0) {
							System.out.println("Adres - Ulica niezmieniona - nie wpisano nowej");
						} else {
							if (newCz.getAdresUlica().trim().length() > 255) {
								System.err.println("BŁĄD: Ulica (adres) zbyt długa (przekracza 255 znaków).");
								throw new HibernateException("");
							} else {
								System.out.println("Nowa ulica (adres): " + newCz.getAdresUlica());
								c.setEmail(newCz.getAdresUlica().trim());
							}
						}
					}
					
					// adresnrdomu
					if (c.getAdresNrDomu().trim().equals(newCz.getAdresNrDomu().trim())) {
						System.out.println("Adres - nr domu niezmieniony - podano taki sam.");
					} else {
						if (newCz.getAdresNrDomu().trim().length() == 0) {
							System.out.println("Adres - nr domu niezmieniono - nie podano nowego.");
						} else {
							if (newCz.getAdresNrDomu().trim().length() > 10) {
								System.err.println("BŁĄD: Nr domu (adres) zbyt długi (przekrzacza 10 znaków)");
								throw new HibernateException("");
							} else {
								System.out.println("Nowy nr domu (adres): " + newCz.getAdresNrDomu());
								c.setAdresNrDomu(newCz.getAdresNrDomu().trim());
							}
						}
					}
					// adresnrmieszkania
					if (c.getAdresNrMieszkania().trim().equals(newCz.getAdresNrMieszkania().trim())) {
						System.out.println("Adres - nr mieszkania niezmieniony - podano taki sam.");
					} else {
						if (newCz.getAdresNrMieszkania().trim().length() == 0) {
							System.out.println("Adres - nr mieszkania niezmieniony - nie podano.");
						} else {
							if (newCz.getAdresNrMieszkania().trim().length() > 10) {
								System.err.println("BŁĄD: Zbyt długi nr mieszkania.");
								throw new HibernateException("");
							} else {
								System.out.println("Nowy nr mieszkania (adres): " + newCz.getAdresNrMieszkania());
								c.setAdresNrMieszkania(newCz.getAdresNrMieszkania().trim());
							}
						}
					}
					// miasto
					if (c.getAdresMiasto().trim().equals(newCz.getAdresMiasto().trim())) {
						System.out.println("Adres - miasto niezmienione - podano takie samo.");
					} else {
						if (newCz.getAdresMiasto().trim().length() == 0) {
							System.out.println("Miasto (adres) niezmieniony - nie podano nowego.");
						} else {
							if (newCz.getAdresMiasto().trim().length() > 30) {
								System.err.println("BŁĄD: Miasto (adres) zbyt długie (przekracza 30 znaków).");
								throw new HibernateException("");
							} else {
								System.out.println("Nowe miasto (adres): " + newCz.getAdresMiasto());
								c.setAdresMiasto(newCz.getAdresMiasto().trim());
							}
						}
					}
					session.update(c);
					tx.commit();
					stan = true;
				} catch (HibernateException e) {
					if (tx != null)
						tx.rollback();
					stan = false;
				} finally {
					session.close();
				}
			}
		return stan;
		}

	public void showAllCzytelnicy() {
		session = factory.openSession();
		tx = null;
		List<Czytelnik> czytelnicy = new ArrayList<Czytelnik>();
		try {
			tx = session.beginTransaction();
			query = session.createQuery("from Czytelnik");
			czytelnicy = query.list();

			tx.commit();

			if (czytelnicy.size() > 0) {
				System.out.println("Czytelnicy w bazie: ");
				System.out
						.println("ID | Imię | Nazwisko | Pesel | E-mail | Ulica | Nr domu | Nr mieszkania | Miasto");

				for (Czytelnik out : czytelnicy) {
					System.out.println(out);
				}
			} else {
				System.out.println("Brak czytelników w bazie.");
			}

		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
		} finally {
			session.close();
		}
	}
}
