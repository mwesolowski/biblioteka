package pl.biblioteka.entityManagers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


import pl.biblioteka.entities.Autor;
import pl.biblioteka.entities.Ksiazka;
import pl.biblioteka.entities.Wydawnictwo;
import pl.biblioteka.main.Start;

public class KsiazkaManager {
	
	private Session session;
	private Session sessionget;
	private Transaction tx;
	private Transaction txget;
	private SessionFactory factory = Start.getFactory();
	private Query query;

	private static KsiazkaManager instance;
	
	public static KsiazkaManager instance() {
		if (instance == null) 
			instance = new KsiazkaManager();
		
		return instance;
	}
	
	private KsiazkaManager() {
	}
	
	public void showAllKsiazki() {
		session = factory.openSession();
		tx = null;
		
		List<Ksiazka> ksiazki = new ArrayList<Ksiazka>();
		
		try {
			tx = session.beginTransaction();
			query = session.createQuery("from Ksiazka");
			
			ksiazki = query.list();
			
			tx.commit();
			
			if (ksiazki.size() > 0) {
				System.out.println("Książki w bazie: ");
				System.out.println("ID | Autor | Data wydania | Tytuł | ISBN | Wydawnictwo | Liczba Stron | Czy wypożyczona");
				for (Ksiazka out : ksiazki) {
					System.out.println(out);
				}
			} else {
				System.out.println("Brak książek w bazie.");
			}
			
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
		} finally {
			session.close();
			
		}
	}
	
	public boolean addKsiazka(int idAutor, String data, String bookTitle, String isbnNumber, int wydID, int liczbaStron) {
		Date parsedDate = null;
		// id autora
		if (idAutor < 0) {
			System.err.println("BŁĄD: Podano nieprawidłowy ID autora.");
			return false;
		} else {
			// sprawdz czy jest w bazie o takim id
			if (AutorManager.instance().getAutor(idAutor) == null) {
				System.err.println("BŁĄD: Autor o takim ID nie istnieje.");
				return false;
			}
		}
		
		// data
		if (data.trim().length() == 0) {
				System.err.println("BŁĄD: Nie podano daty.");
				return false;
			} else {
				if (parseDate(data) == null) {
					System.err.println("BŁĄD: Podano nieprawidłową datę.");
					return false;
				} else {
					parsedDate = parseDate(data);
				}
			}
		// tytul ksiazki
		if (bookTitle.trim().length() == 0) {
				System.err.println("BŁĄD: Nie podano tytułu książki.");
				return false;
		} else {
			if (bookTitle.trim().length() > 255) {
				System.err.println("BŁĄD: Tytuł książki zbyt długi (przekrzacza 255 znaków)");
				return false;
			}
		}
		
		// isbn
		if (isbnNumber.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano nr ISBN książki.");
			return false;
		} else {
			if (isbnNumber.trim().length() != 13) {
				System.err.println("BŁĄD: Numer ISBN nie zawiera 13 znaków.");
				return false;
			} else {
				// sprawdz czy juz jest w bazie ksiazka o takim isbn
				if (getKsiazka(isbnNumber) != null) {
					System.err.println("BŁĄD: Książka o takim nr ISBN już istnieje w bazie.");
					return false;
				}
			}
		}
		
		// id_wydawnictwo
		if (wydID < 0) {
			System.err.println("BŁĄD: Podano nieprawidłowy ID wydawnictwa.");
			return false;
		} else {
			// sprawdz czy istnieje autor o takim id w bazie
			if (WydawnictwoManager.instance().getWydawnictwo(wydID) == null) {
				System.err.println("BŁĄD: Wydawnictwo o takim ID nie istnieje w bazie.");
				return false;
			}
		}
		
		// liczba stron
		if (liczbaStron <= 0) {
			System.err.println("BŁĄD: Liczba stron nie może być równa zero lub mniej.");
			return false;
		}
				
		tx = null;
		session = factory.openSession();
		
		try {
			tx = session.beginTransaction();
			
			Autor au = new Autor();
			au.setId(idAutor);
			
			Wydawnictwo wy = new Wydawnictwo();
			wy.setId(wydID);
			
			Ksiazka ksiazka = new Ksiazka();
			ksiazka.setAutor(au);
			ksiazka.setDataWydania(parsedDate);
			ksiazka.setTytul(bookTitle.trim());
			ksiazka.setIsbn(isbnNumber.trim());
			ksiazka.setWydawnictwo(wy);
			ksiazka.setLiczbaStron(liczbaStron);
			
			session.save(ksiazka);
			
			tx.commit();
			System.out.println("Książka dodana do bazy.");
			return true;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			return false;
		} finally {
			session.close();
		}
		
	}
	
	public Date parseDate(String dataString) {
		Date dateParsed = null;
		
		DateFormat df = new SimpleDateFormat("MM/yyyy");
		String delimiter = "/";
		try {
			// zapisuje rok i miesiac do osobnych miejsc w tablicy
			String temp[] = dataString.split(delimiter);
			int mies_rok[] = new int[2];
			for (int i=0; i< temp.length; i++) {
				mies_rok[i] = Integer.parseInt(temp[i]);
			}
			
			// sprawdzam czy dobry miesiac podany
			if (mies_rok[0] >1 && mies_rok[0] <=12) {
			} else {
				System.err.println("BŁĄD: Podano nieprawidłowy miesiąc.");
				throw new ParseException("", 0);
			}
			
			// sprawdzam czy dobry rok podany
			if (mies_rok[1] >0 && mies_rok[1] <= 9999) {
			} else {
				System.err.println("BŁĄD: Podano nieprawidłowy rok.");
				throw new ParseException("",0);
			}
			
			
			dateParsed = df.parse(dataString);
		} catch (ParseException e) {
			System.err.println("BŁĄD: Wystąpił błąd podczas parsowania daty.");
			
			dateParsed = null;
		}
		return dateParsed;
	}
	
	public Ksiazka getKsiazka(String isbn) {

		Ksiazka ks = new Ksiazka();
		
		if (isbn.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano nr ISBN książki.");
			return ks;
		} else
			if (isbn.trim().length() != 13) {
				System.err.println("BŁĄD: Podano nieprawidłowy ISBN książki.");
				return ks;
			} else {
				sessionget = factory.openSession();
				txget = null;
			
				try {
					txget = sessionget.beginTransaction();
					query = sessionget.createQuery("from Ksiazka where isbn=:isbn").setParameter("isbn", isbn.trim());
					ks = (Ksiazka) query.uniqueResult();
					
					txget.commit();
					
					if (ks == null) {
						System.out.println("Książka o podanym ISBN nie istnieje w bazie.");
					} else {
						System.out.println("Dane książki:");
						System.out.println("ID| Autor| Data wydania| Tytuł| ISBN| Wydawnictwo| Liczba stron| Czy wypożyczona");
						System.out.println(ks);
					}
					
				} catch (HibernateException e) {
					if (txget != null)
						txget.rollback();
				} finally {
					sessionget.close();
				}
		}
		return ks;
	}
	public Ksiazka getKsiazka(int id) {
		Ksiazka ks = new Ksiazka();
		
		if (id < 0) {
			System.err.println("BŁĄD: Podano nieprawidłowe ID książki.");
			return ks;
		} else {
			sessionget = factory.openSession();
			tx = null;
			
			try {
				tx = sessionget.beginTransaction();
				ks = (Ksiazka) sessionget.get(Ksiazka.class, id);
				
				tx.commit();
				if (ks == null) {
					System.err.println("BŁĄD: Książka o ID " + id + " nie istnieje w bazie.");
				}
			} catch (HibernateException e) {
				if (tx != null)
					tx.rollback();
			} finally {
				sessionget.close();
			}
		}
		return ks;
	}
	public void getKsiazkaByTitle(String tytul) {
		
		List<Ksiazka> ksList = new ArrayList<Ksiazka>();
		
		if (tytul.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano tytułu książki.");
		} else {
			if (tytul.trim().length() > 255) {
				System.err.println("BŁĄD: Tytuł książki nie może przekraczać 255 znaków.");
			} else {
				session = factory.openSession();
				tx = null;
				
				try {
					tx = session.beginTransaction();
					query = session.createQuery("from Ksiazka where upper(tytul) like upper(:tytul)");
					ksList = query.setParameter("tytul", tytul.trim() + "%").list();
					
					tx.commit();
					
					if (ksList.size() == 0) {
						System.out.println("Nie znaleziono książki o podanym tytule.");
					} else {
						System.out.println("Wyniki wyszukiwania: ");
						System.out.println("ID| Autor| Data wydania| Tytuł| ISBN| Wydawnictwo| Liczba stron| Czy wypożyczona");
						for (Iterator it = ksList.iterator(); it.hasNext();) {
							Ksiazka ks = (Ksiazka) it.next();
							System.out.println(ks);
						}
					}
				} catch (HibernateException e) {
					if (tx != null) 
						tx.rollback();
					
					e.printStackTrace();
				} finally { 
					session.close();
				}
			}
		}
	}
	
	public boolean editKsiazka(int ksiazkaID, Ksiazka newKsi) {
		boolean stan = false;
		if (ksiazkaID < 0) {
			System.err.println("BŁĄD: Nieprawidłowy ID książki do aktualizacji danych.");
			return stan;
		} else {
			session = factory.openSession();
			tx = null;
			
			try {
				tx = session.beginTransaction();
				Ksiazka ks = (Ksiazka) session.get(Ksiazka.class, ksiazkaID);
				
				if (ks == null) {
					System.err.println("BŁĄD: Książka o numerze ID: " + ksiazkaID + " nie istnieje w bazie.");
					throw new HibernateException("");
				}
				
				// id autora
				if (newKsi.getAutor().getId() < 0) {
					System.err.println("BŁĄD: Podano nieprawidłowy nowy ID autora.");
					throw new HibernateException("");
				} else {
					if(ks.getAutor().getId() == newKsi.getAutor().getId()) {
						System.out.println("Autor niezmieniony. Wpisano ten sam ID.");
					} 
				}
				
				// data wydania
				if (newKsi.getDataWydania() == null) {
					System.err.println("BŁĄD: Nowa data wydania jest nieprawidłowa.");
					throw new HibernateException("");
				} else {
					if (ks.getDataWydania() == newKsi.getDataWydania()) {
						System.out.println("Data wydania niezmieniona. Wpisano tą samą.");
					} 					
				}
				
				// tytul
				if (ks.getTytul().trim().equals(newKsi.getTytul().trim())) {
					System.out.println("Tytuł niezmieniony. Podano taki sam.");
				} else {
					if (newKsi.getTytul().trim().length() == 0) {
						System.out.println("Tytuł niezmieniony. Nie podano nowego.");
					} else {
						if (newKsi.getTytul().trim().length() > 255) {
							System.err.println("BŁĄD: Tytuł zbyt długi. Przekracza 255 znaków.");
							throw new HibernateException("");
						} else {
							System.out.println("Nowy tytuł: " + newKsi.getTytul().trim());
						}
					}
				}
				
				// isbn
				if (ks.getIsbn().trim().equals(newKsi.getIsbn().trim())) {
					System.out.println("ISBN niezmieniony. Podano taki sam.");
				} else {
					if (newKsi.getIsbn().trim().length() == 0) {
						System.out.println("ISBN niezmieniony. Nie podano nowego.");
					} else {
						if (newKsi.getIsbn().trim().length() != 13 ) {
							System.err.println("BŁĄD: ISBN musi mieć dokładnie 13 cyfr.");
							throw new HibernateException("");
						} else {
							System.out.println(getKsiazka(newKsi.getIsbn().trim()));
							if (getKsiazka(newKsi.getIsbn().trim()) != null) {
								System.out.println("BŁĄD: Próba zmiany ISBN na inny, już istniejący.");
								throw new HibernateException("");
							}
						}							
					}
				}
				
				// id wydawnictwo
				if (newKsi.getWydawnictwo().getId() < 0) {
					System.err.println("BŁĄD: Podano nieprawidłowy nowy ID wydawnictwa.");
					throw new HibernateException("");
				} else {
					if (ks.getWydawnictwo().getId() == newKsi.getWydawnictwo().getId()) {
						System.out.println("Wydawnictwo niezmienione. Podano ID to samo.");
					}
				}
				
				
				
			} catch (HibernateException e) {
				if (tx != null)
					tx.rollback();
			} finally {
				session.close();
			}
		}
		return stan;
	}
	public boolean deleteKsiazka(int ksiazkaID) {
		boolean stan = false;
		
		if (ksiazkaID < 0) {
			System.err.println("BŁĄD: Podano nieprawidłowy ID książki do usunięcia.");
			return stan;
		} else {
			session = factory.openSession();
			tx = null;
			
			try {
				tx = session.beginTransaction();
				Ksiazka ks = (Ksiazka) session.get(Ksiazka.class, ksiazkaID);
				
				if (ks == null) {
					System.err.println("BŁĄD: Książka o ID " + ksiazkaID + " nie istnieje w bazie.");
					throw new HibernateException("");
				}
				
				session.delete(ks);
				tx.commit();
				System.out.println("Pomyślnie usunięto książkę o ID: " + ksiazkaID);
				stan = true;
			} catch (HibernateException e) {
				if (tx != null)
					tx.rollback();
				
				stan = false;
			} finally {
				session.close();
			}
		}
		return stan;
	}
}
