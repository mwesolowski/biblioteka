/*
 * Klasa zarządzająca tabelą "Autorzy" w bazie biblioteki.
 * Zaimplementowane metody:
 * - dodawanie autora
 * - modyfikacja danych nt autora
 * - usuwanie autora
 * - wyświetlanie wszystkich autorów w bazie
 * - wyszukiwanie autorów po nr ID w bazie
 * - wyszukiwanie autorów po nazwie w bazie
 */

package pl.biblioteka.entityManagers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import pl.biblioteka.entities.Autor;
import pl.biblioteka.main.Start;

public class AutorManager {

	private Session session;
	private Transaction tx;
	private SessionFactory factory = Start.getFactory();
	private Query query;
	
	private static AutorManager instance;

	public static AutorManager instance() {
		if (instance == null) 
			instance = new AutorManager();
		
		return instance;
	}
	private AutorManager() {
	}

	// Dodawanie autora do bazy
	public int addAutor(String autName) {
		if (autName.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano nazwy autora.");
			return -1;
		} else {
			if (autName.trim().length() > 255) {
				System.err.println("BŁĄD: Nazwa autora nie może przekraczać 255 znaków.");
				return -1;
			} else {
				List<Autor> autList = searchAutor(autName);
				
				if (autList.size() > 0) {
					for (Autor out : autList) {
						if (out.getNazwa().trim().equals(autName.trim())) {
							System.err.println("BŁĄD: Autor o podanej nazwie istnieje w bazie.");
							return -1;
						}
					}
				}
			}
		}
		
		session = factory.openSession();
		tx = null;
		Integer autID = null;
		
		try {
			tx = session.beginTransaction();
			Autor aut = new Autor(autName.trim());
			autID = (Integer) session.save(aut);
			tx.commit();
			System.out.println("Autor o nazwie " + autName + " dodany do bazy danych.");
		} catch (HibernateException e) {
			if (tx != null) 
				tx.rollback();
			
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return autID;
			
	}
	// Wyswietlanie wszystkich autorow zapisanych w bazie
	public void listAuthors() {
		session = factory.openSession();
		tx = null;
		
		try {
			tx = session.beginTransaction();
			List autorzy = session.createQuery("FROM Autor").list();
			if (autorzy.size() > 0) {
				System.out.println("Autorzy zapisani w bazie: ");
		    	System.out.println("ID | Nazwa");
		    	
				for (Iterator iterator = autorzy.iterator(); iterator.hasNext();) {
					Autor autor = (Autor) iterator.next();
					System.out.print(autor.getId());
					System.out.println("| " + autor.getNazwa());
				}
			} else {
				System.out.println("Brak autorów zapisanych w bazie.");
			}
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	// Zmiana nazwy autora
	public boolean updateAutor(int autID, String nazwa) {
		if (autID < 0) {
			System.err.println("BŁĄD: Podano nieprawidłowy nr ID autora do zmiany nazwy. ID autora jest liczbą całkowitą dodatnią.");
			return false;
		}
		if (nazwa.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano nowej nazwy autora.");
			return false;
		} else {
			if (nazwa.trim().length() > 255) {
				System.err.println("BŁĄD: Nowa nazwa autora nie może przekraczać 255 znaków.");
				return false;
			}
		}
		
		session = factory.openSession();
		tx = null;
		
		try {
			tx = session.beginTransaction();
			Autor autor = (Autor) session.get(Autor.class, autID);
			
			if (autor == null) {
				System.err.println("BŁĄD: Autor o numerze ID " + autID + " nie istnieje w bazie.");
				throw new HibernateException("");
			}
			
			if (autor.getNazwa().trim().equals(nazwa.trim()) ) {
				System.err.println("BŁĄD: Nowa nazwa autora jest taka sama jak stara nazwa.");
				throw new HibernateException("");
			}
			autor.setNazwa(nazwa.trim());
			
			session.update(autor);
			tx.commit();
			
			System.out.println("Nazwa autora zmieniona.");
			return true;
		} catch(HibernateException e) {
			if (tx != null)
				tx.rollback();
			
			return false;
		} finally {
			session.close();
		}
	}
	
	// kasowanie autora
	public boolean deleteAutor(int autID) {
		if (autID < 0) {
			System.err.println("BŁĄD: Podano nieprawidłowy nr ID autora do zmiany nazwy. ID autora jest liczbą całkowitą dodatnią.");
			return false;
		}
		
		session = factory.openSession();
		tx = null;
		
		try {
			tx = session.beginTransaction();
			Autor autor = (Autor) session.get(Autor.class, autID);
			
			if (autor == null) {
				System.err.println("BŁĄD: Autor o numerze ID " + autID + " nie istnieje w bazie.");
				throw new HibernateException("");
			}
			
			session.delete(autor);
			tx.commit();
			return true;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			
		
			return false;
		} finally {
			session.close();
		}
	}
	
	// wyszukiwanie autora po nazwie - zwraca liste dla addAutor
	private List<Autor> searchAutor(String autName) {
		List<Autor> autorzy = new ArrayList<Autor>();
		
		if (autName.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano nazwy autora do wyszukania.");
			return autorzy;
		} else {
			if (autName.trim().length() > 255) {
				System.err.println("BŁĄD: Nazwa autora nie może przekraczać 255 znaków.");
				return autorzy;
			}
		}
		
		session = factory.openSession();
		tx = null;
		
		try {
			tx = session.beginTransaction();
			query = session.createQuery("FROM Autor WHERE UPPER(nazwa) LIKE UPPER(:autName)");
			autorzy = query.setParameter("autName", "%" + autName.trim() + "%").list();
			tx.commit();
			
			return autorzy;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			
			e.printStackTrace();
			return autorzy;
		} finally {
			session.close();
		}
	}
	
	// zwykle przeszukiwanie bez zwracania listy
	public void searchAutorWithoutList(String autName) {
		if (autName.trim().length() == 0) {
			System.err.println("BŁĄD: Nie podano nazwy autora do wyszukania.");
			return;
		} else {
			if (autName.trim().length() > 255) {
				System.err.println("BŁĄD: Nazwa autora nie może przekraczać 255 znaków.");
				return;
			}
		}
		
		session = factory.openSession();
		tx = null;
		
		List<Autor> autorzy = new ArrayList<Autor>();
		
		try {
			tx = session.beginTransaction();
			query = session.createQuery("FROM Autor WHERE UPPER(nazwa) like UPPER(:autName)");
			autorzy = query.setParameter("autName", "%" + autName.trim() + "%").list();
			
			tx.commit();
			
			if (autorzy.size() == 0) {
				System.out.println("Nie znaleziono autora o podanej nazwie.");
			} else {
				System.out.println("Wyniki wyszukiwania:");
				System.out.println("ID| Nazwa");
				for (Iterator iterator=autorzy.iterator(); iterator.hasNext();) {
					Autor aut = (Autor) iterator.next();
					System.out.print(aut.getId());
					System.out.println("| " + aut.getNazwa());
				}
			}
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			
			e.printStackTrace();
		} finally {
			
			session.close();
		}
	}
	
	// wyciaganie autora o podanym ID
	public Autor getAutor(int autID) {
		Autor autor = new Autor();
		
		session = factory.openSession();
		tx = null;
		
		try {
			tx = session.beginTransaction();
			autor = (Autor) session.get(Autor.class, autID);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) 
				tx.rollback();
			
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return autor;
	}
	
	public void setFactory(SessionFactory f) {
		factory = f;
	}

}