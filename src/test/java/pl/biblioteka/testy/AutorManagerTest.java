package pl.biblioteka.testy;

import static org.junit.Assert.*;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.junit.*;

import pl.biblioteka.entityManagers.AutorManager;

public class AutorManagerTest {
	
	AutorManager am = AutorManager.instance();

	private static SessionFactory factory;
	private static ServiceRegistry serviceRegistry;
	
	@BeforeClass
	public static void initHibernate() {
		try {
			Configuration configuration = new Configuration();
    		configuration.configure();
		    		
    		serviceRegistry = new ServiceRegistryBuilder().applySettings(
    				configuration.getProperties()).buildServiceRegistry();
    		
    		factory=configuration.buildSessionFactory(serviceRegistry); 
    		
		} catch (Throwable e) {
			System.err.println("Failed to create sessionFactory object." + e);
			throw new ExceptionInInitializerError(e);
		}
	}
	@Test
	public void testAutorNameIsEmpty() {
		assertEquals(-1, am.addAutor(""));
	}
	
	@Test
	public void testAutorNameIsEmptySpaces() {
		assertEquals(-1, am.addAutor("   "));
	}
	
	@Test
	public void autorNameTooLong() {
		assertEquals(-1,am.addAutor("26237434987787677472262374349877876774722623743498778767747226237434987787677472" +
				"2623743498778767747226237434987787677472262374349877876774722623743498778767747226237434987787677472" +
				"2623743498778767747226237434987787677472262374349877876774722623743498778767747226237434987787677472" +
				"2623743498778767747226237434987787677472262374349877876774722623743498778767747226237434987787677472"));
	}
	
	@Test
	public void addAuthor() {
		am.setFactory(factory);
		
		// zwroc fail jesli autor istnieje
		if (am.addAutor("www") == -1 ) {
			fail();
		}
		// daj znac ze sie powiodlo jesli autor sie dodal
		else {
			assertTrue(true);
		}
	}
}